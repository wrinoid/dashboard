<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class MY_Controller extends CI_Controller
{
    public $request;
    public $auth;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_system');

        $this->parseRequest();
    }

    public function prepareData($title = "", $menu)
    {
        $data['title'] = $title;
        $data['menu'] = $menu;

        $total_visitor = $this->M_system->count('t_visitors');
        $unit = ['', 'K', 'M', 'B'];
        $selected_unit_key = floor((strlen($total_visitor) - 1) / 3);
        $short = $total_visitor / (int) str_pad('1', ($selected_unit_key * 3) + 1, '0');
        $formated_short = (floor($short * 100) / 100) . $unit[$selected_unit_key];
        $data['total_visitors'] = $formated_short;

        return $data;
    }

    public function authentication()
    {
        if ($this->input->cookie('token') != '') {
            $decoded = JWT::decode($this->input->cookie('token'), new Key($_ENV['JWT_SECRET'], 'HS256'));

            $user = $this->M_system->get('t_users', ['user_id' => $decoded->sub->id])->row();

            if ($user) {
                $exp = '';
                $remember = false;
                if ($decoded->sub->remember == true) {
                    $exp = '2592000';
                    $remember = true;
                } else {
                    $exp = '21600';
                }

                // $cookie = [
                //     'name'   => 'remember',
                //     'value'  => $remember,
                //     'expire' => $exp,
                // ];
                // $this->input->set_cookie($cookie);
                // $cookie = [
                //     'name'   => 'login',
                //     'value'  => true,
                //     'expire' => $exp,
                // ];
                // $this->input->set_cookie($cookie);
                // $cookie = [
                //     'name'   => 'username',
                //     'value'  => $user->username,
                //     'expire' => $exp,
                // ];
                // $this->input->set_cookie($cookie);
                // $cookie = [
                //     'name'   => 'email',
                //     'value'  => $user->email,
                //     'expire' => $exp,
                // ];
                // $this->input->set_cookie($cookie);
                // $cookie = [
                //     'name'   => 'name',
                //     'value'  => $user->name,
                //     'expire' => $exp,
                // ];
                // $this->input->set_cookie($cookie);
                // $cookie = [
                //     'name'   => 'id',
                //     'value'  => $user->user_id,
                //     'expire' => $exp,
                // ];
                // $this->input->set_cookie($cookie);

                $payload = [
                    'iss' => 'admin',
                    'sub' => [
                        'remember' => $remember,
                        'login' => true,
                        'username' => $decoded->sub->username,
                        'id' => $decoded->sub->id
                    ],
                    'iat' => time(),
                    'exp' => time() + $exp,
                ];
                $jwt = JWT::encode($payload, $_ENV['JWT_SECRET'], 'HS256');

                $cookie = [
                    'name'   => 'token',
                    'value'  => $jwt,
                    'expire' => $exp,
                ];
                $this->input->set_cookie($cookie);

                if ($user->gdrive_token == null) {
                    redirect('oauth2');
                }

                $this->auth = $user;

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getClient($id)
    {
        $user = $this->M_system->get('t_users', ['user_id' => $id])->row();

        if ($user->gdrive_token) {
            $client = new Google_Client();
            $client->setAuthConfigFile(FCPATH . 'client_secret.json');
            $client->setAccessToken($user->gdrive_token);
            $client->setAccessType('offline');
            if ($client->isAccessTokenExpired()) {
                $client->fetchAccessTokenWithRefreshToken($user->gdrive_refresh_token);
                $client->setAccessToken(json_encode($client->getAccessToken()));
                $client->addScope(Google_Service_Sheets::DRIVE);
                $client->addScope(Google_Service_Sheets::DRIVE_FILE);
                $client->addScope(Google_Service_Sheets::DRIVE_READONLY);
                $client->addScope(Google_Service_Sheets::SPREADSHEETS);
                $client->addScope(Google_Service_Sheets::SPREADSHEETS_READONLY);

                $this->M_system->update('t_users', ['user_id' => $id], ['gdrive_token' => json_encode($client->getAccessToken())]);
            }

            return $client;
        }

        return false;
    }

    public function parseRequest()
    {
        $this->request = $_POST;
    }
}
