<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| SendGrid Setup
| -------------------------------------------------------------------
*/

$config['protocol']         = 'smtp';
$config['smtp_host']        = $_ENV['SMTP_HOST'];
$config['smtp_user']        = $_ENV['SMTP_USER'];
$config['smtp_pass']        = $_ENV['SMTP_PASS'];
$config['smtp_port']        = $_ENV['SMTP_PORT'];
$config['smtp_crypto']      = 'ssl';
$config['newline']          = "\r\n";