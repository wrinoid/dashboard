<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use Carbon\Carbon;

class Accounting extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->authentication()) {
            redirect('auth/login');
        }

        $this->load->model('M_account');
        $this->load->model('M_category');
        $this->load->model('M_journal');
    }

    public function dashboard()
    {
        $data = $this->prepareData('Accounting', 'accounting_dashboard');

        $data['modal'] = $this->M_journal->getModal()->amount;
        $data['saldo'] = $this->M_account->getSaldo()->balance;

        $margin = $data['saldo'] - $data['modal'];
        $data['margin_percentage'] = number_format($margin / $data['modal'] * 100, 2);

        $line_chart = $this->M_journal->getLineChart(Carbon::now()->month, Carbon::now()->year);
        $total_hari = date('t');

        $k = 0;
        $data['line_chart']['day'] = $data['line_chart']['value'] = [];
        for ($i=1; $i<=$total_hari; $i++){
            $data['line_chart']['day'][] = $i;
            $data['line_chart']['value'][$i] = 0;
            
            if (count($line_chart) > 0 && array_key_exists($k, $line_chart) && $line_chart[$k]->tanggal == $i) {
                $data['line_chart']['value'][$i] = $line_chart[$k]->amount;
                $k++;
            }
        }

        $data['line_chart']['day'] = "[".implode(',', $data['line_chart']['day'])."]";
        $data['line_chart']['value'] = "[".implode(',', $data['line_chart']['value'])."]";

        return view('accounting/dashboard', $data);
    }

    public function account()
    {
        $data = $this->prepareData('Accounting', 'accounting_account');

        return view('accounting/account', $data);
    }

    public function category()
    {
        $data = $this->prepareData('Accounting', 'accounting_category');

        return view('accounting/category', $data);
    }

    public function journal()
    {
        $data = $this->prepareData('Accounting', 'accounting_journal');

        $data['accounts'] = $this->M_account->get()->result();
        $data['categories'] = $this->M_category->get(['type' => 'Debit'])->result();

        return view('accounting/journal', $data);
    }
}
