<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Backup extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function database()
    {
        // db : system
        $this->load->dbutil();
        $prefs = [
            'format' => 'zip',
            'filename' => 'wrino_system_' . date("Y-m-d-H-i-s") . '.sql'
        ];
        $this->dbutil->backup($prefs);

        // db : berkas
        $db_berkas = $this->load->database('berkas', true);
        $this->load->dbutil($db_berkas);
        $prefs = [
            'format' => 'zip',
            'filename' => 'wrino_berkas_' . date("Y-m-d-H-i-s") . '.sql'
        ];
        $berkas = $this->dbutil->backup($prefs);

        $this->load->helper('file');
        write_file(FCPATH.'/backup/wrino_database.zip', $berkas);

        $this->load->library('email');

        $this->email->from('mail@wrino.id', 'mail@wrino.id');
        $this->email->to('winapamungkasr@gmail.com');

        $this->email->subject('Backup database ' . date("Y-m-d-H-i-s"));

        $this->email->attach(FCPATH.'/backup/wrino_database.zip');

        if (!$this->email->send(false)) {
            print_r($this->email->print_debugger());
        }
    }

    public function file()
    {
        $array = ['root', 'dashboard', 'berkas'];

        // load zip library
        $this->load->library('recurseZip_lib');
        // load email library
        // $this->load->library('email');

        foreach ($array as $backup) {
            // $this->email->clear(TRUE);
            // $this->email->from('mail@wrino.id', 'mail@wrino.id');
            // $this->email->to('winapamungkasr@gmail.com');

            // $this->email->subject('Backup file '.date("Y-m-d"));

            if ($backup == 'root') {
                $opt = [
                    'src' => '../../public_html/assets/',
                    'dst' => '../../public_html/backup/files/'
                ];
                $this->recursezip_lib->options($opt);
                $this->recursezip_lib->compress(true);

                // $this->email->attach('../../public_html/backup/files/root.zip');
            } else {
                $opt = [
                    'src' => '../../public_html/' . $backup . '/',
                    'dst' => '../../public_html/backup/files/'
                ];
                $this->recursezip_lib->options($opt);
                $this->recursezip_lib->compress();

                // $this->email->attach('../../public_html/backup/files/'.$backup.'.zip');
            }

            // if(!$this->email->send(FALSE)){
                // print_r($this->email->print_debugger());
            // }
        }
    }
}
