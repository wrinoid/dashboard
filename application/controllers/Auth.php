<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Auth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_system');
    }

    public function login()
    {
        if ($this->authentication()) {
            redirect('welcome');
        }

        if ($this->input->post('submit')) {
            $user = $this->M_system->get('t_users', ['username' => $this->input->post('username')])->row();

            if ($user) {
                if (password_verify($this->input->post('password'), $user->password)) {
                    $exp = '';
                    $remember = false;
                    if ($this->input->post('remember')) {
                        $exp = '2592000';
                        $remember = true;
                    } else {
                        $exp = '21600';
                    }

                    $payload = [
                        'iss' => 'admin',
                        'sub' => [
                            'remember' => $remember,
                            'login' => true,
                            'username' => $user->username,
                            'id' => $user->user_id,
                            'photo' => $user->photo
                        ],
                        'iat' => time(),
                        'exp' => time() + $exp,
                    ];
                    $jwt = JWT::encode($payload, $_ENV['JWT_SECRET'], 'HS256');

                    $cookie = [
                        'name'   => 'token',
                        'value'  => $jwt,
                        'expire' => $exp,
                    ];
                    $this->input->set_cookie($cookie);

                    redirect('welcome');
                } else {
                    $this->session->set_flashdata('login', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><span>User and Password didn\'t match.</span></div>');
                }
            } else {
                $this->session->set_flashdata('login', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><span>User and Password didn\'t match.</span></div>');
            }
        }

        return view('auth.login');
    }

    public function logout()
    {
        $cookie = [
            'name'   => 'token',
            'value'  => '',
            'expire' => 0,
        ];
        $this->input->set_cookie($cookie);

        redirect('auth/login');
    }
}
