<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Profile extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->authentication()) {
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data = $this->prepareData('Profile', '');



        return view('profile', $data);
    }
}
