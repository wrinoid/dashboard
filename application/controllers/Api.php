<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Api extends MY_Controller
{
    public function getTotalVisitors()
    {
        $total_visitor = $this->M_system->count('t_visitors');

        $unit = ['', 'K', 'M', 'B'];
        $selected_unit_key = floor((strlen($total_visitor) - 1) / 3);
        $short = $total_visitor / (int) str_pad('1', ($selected_unit_key * 3) + 1, '0');
        $formated_short = (floor($short * 100) / 100) . $unit[$selected_unit_key];

        $budget = $this->getBudgeting();

        echo json_encode(
            [
            'budget' => $budget,
            'total' => [
                'long' => number_format($total_visitor, 0, '', '.'),
                'short' => $formated_short
            ],
            'visitors' => $this->M_system->get_visitor_datatable(10, 0, 'visitor_id', 'desc')
            ]
        );
    }

    public function getBudgeting()
    {
        $client = $this->getClient(1);

        if ($client !== false) {
            $service = new Google_Service_Sheets($client);

            $spreadsheetId = '1QmPsjX4U3H29VitutdVH5WW3WkYMJYPrEcvXeWBs9M8';
            $ranges = [
                'ranges' => [
                    "'Target Budgeting'!G1",
                    "'Budget Bulanan'!F1",
                    "'Budget Main'!F1",
                    "'Budget Bulanan'!G1",
                    "'Budget Main'!G1"
                ]
            ];

            $response = $service->spreadsheets_values->batchGet($spreadsheetId, $ranges);

            return [
                'monthly' => $response['valueRanges'][1]['values'][0][0],
                'percentage_monthly' => $response['valueRanges'][3]['values'][0][0],
                'dating' => $response['valueRanges'][2]['values'][0][0],
                'percentage_dating' => $response['valueRanges'][4]['values'][0][0],
                'investing' => $response['valueRanges'][0]['values'][0][0]
            ];
        }
    }
}
