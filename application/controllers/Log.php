<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Log extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->authentication()) {
            redirect('auth/login');
        }
    }

    public function visitor()
    {
        $data = $this->prepareData('Log Visitor', 'log-visitor');

        return view('log.visitor', $data);
    }
}
