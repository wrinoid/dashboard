<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'vendor/autoload.php';

class Oauth2 extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $client = new Google_Client();
        $client->setAuthConfigFile(FCPATH . 'client_secret.json');
        $client->setRedirectUri('https://dashboard.wrino.id/oauth2');
        $client->addScope(Google_Service_Drive::DRIVE);
        $client->addScope(Google_Service_Sheets::DRIVE);
        $client->addScope(Google_Service_Sheets::DRIVE_FILE);
        $client->addScope(Google_Service_Sheets::DRIVE_READONLY);
        $client->addScope(Google_Service_Sheets::SPREADSHEETS);
        $client->addScope(Google_Service_Sheets::SPREADSHEETS_READONLY);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');

        if (!isset($_GET['code'])) {
            $auth_url = $client->createAuthUrl();
            redirect(filter_var($auth_url, FILTER_SANITIZE_URL));
        } else {
            $client->authenticate($_GET['code']);
            $this->M_system->update('t_users', ['user_id' => $this->auth->user_id], ['gdrive_token' => json_encode($client->getAccessToken()), 'gdrive_refresh_token' => $client->getRefreshToken()]);
            redirect();
        }
    }
}
