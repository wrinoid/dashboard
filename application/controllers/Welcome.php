<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use Carbon\Carbon;

class Welcome extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->authentication()) {
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data = $this->prepareData('Dashboard', 'dashboard');

        $data['sheet'] = $this->getSheetData();

        return view('dashboard', $data);
    }

    public function getSheetData()
    {
        try {
            $client = $this->getClient($this->auth->user_id);

            if ($client !== false) {
                $ranges = ["'Target Budgeting'!G1"];

                $idxBudget = [1, 1];
                $rangeBudget = [];
                for ($i = 3; $i <= 17; $i++) {
                    $rangeBudget[] = "'Budgeting'!A".$i;
                    $rangeBudget[] = "'Budgeting'!B".$i;
                    $rangeBudget[] = "'Budgeting'!C".$i;

                    $idxBudget[1] += 3;
                }

                $idxTarget = [$idxBudget[1], $idxBudget[1]];
                $rangeTarget = [];
                for ($i = 2; $i <= 6; $i++) {
                    $rangeTarget[] = "'Target Budgeting'!A".$i;
                    $rangeTarget[] = "'Target Budgeting'!B".$i;
                    $rangeTarget[] = "'Target Budgeting'!C".$i;

                    $idxTarget[1] += 3;
                }

                $ranges = array_merge($ranges, $rangeBudget, $rangeTarget);

                $service = new Google_Service_Sheets($client);

                $spreadsheetId = '1QmPsjX4U3H29VitutdVH5WW3WkYMJYPrEcvXeWBs9M8';
                $data = $service->spreadsheets_values->batchGet(
                    $spreadsheetId, [
                        "ranges" => $ranges
                    ]
                );

                $cashInHand = $data['valueRanges'][0]['values'][0][0];

                $budgets = [];
                for ($i = $idxBudget[0]; $i < $idxBudget[1]; $i += 3) {
                    $budgets[slug($data['valueRanges'][$i]['values'][0][0])] = [
                        'name' => $data['valueRanges'][$i]['values'][0][0],
                        'budget' => str_replace(['Rp', ','], ['', ''], $data['valueRanges'][$i + 1]['values'][0][0]),
                        'value' => str_replace(['Rp', ','], ['', ''], $data['valueRanges'][$i + 2]['values'][0][0]),
                    ];
                }

                $targets = [];
                for ($i = $idxTarget[0]; $i < $idxTarget[1]; $i += 3) {
                    $targets[slug($data['valueRanges'][$i]['values'][0][0])] = [
                        'name' => $data['valueRanges'][$i]['values'][0][0],
                        'budget' => str_replace(['Rp', ','], ['', ''], $data['valueRanges'][$i + 1]['values'][0][0]),
                        'value' => str_replace(['Rp', ','], ['', ''], $data['valueRanges'][$i + 2]['values'][0][0]),
                    ];
                }

                $total = [
                    'budget' => 0,
                    'value' => 0,
                ];

                $color = ['#00a65a', '#f5c954', '#ffac74', '#f56954', '#f56954'];
                $bg_color = ['bg-success', 'bg-warning', 'bg-orange', 'bg-danger', 'bg-danger'];

                foreach ($budgets as $key => $value) {
                    $budgets[$key]['percentage'] = floor($value['budget'] - $value['value'] > 0 ? ($value['value'] / $value['budget'] * 100) : 100);
                    $budgets[$key]['color'] = $color[floor($budgets[$key]['percentage'] / 25)];
                    $budgets[$key]['bg_color'] = $bg_color[floor($budgets[$key]['percentage'] / 25)];

                    $total['budget'] += (float) $value['budget'];
                    if ($key != 'sisa') $total['value'] += (float) $value['value'];
                }

                foreach ($targets as $key => $value) {
                    $targets[$key]['percentage'] = floor($value['value'] / $value['budget'] * 100);
                    $targets[$key]['color'] = $color[4 - ceil($targets[$key]['percentage'] / 25)];
                    $targets[$key]['bg_color'] = $bg_color[4 - ceil($targets[$key]['percentage'] / 25)];
                }

                $num_days = date('t'); // total days of current month
                $cur_days = date('j'); // current date of current month

                $usage['budget'] = ((($num_days - $cur_days) / $num_days) * $budgets['bulanan']['budget']); // sisa seharusnya
                $usage['value'] = $budgets['bulanan']['budget'] - $budgets['bulanan']['value']; // sisa
                $usage['name'] = ($usage['value'] < $usage['budget'] ? 'Boros' : ($usage['value'] > $usage['budget'] ? 'Hemat' : ''));
                $usage['per_day'] = ($usage['value'] <= 0) ? 0 : ($usage['value'] / (Carbon::now()->diffInDays(Carbon::now()->endOfMonth()) == 0 ? 1 : Carbon::now()->diffInDays(Carbon::now()->endOfMonth())));

                $usage_main['budget'] = ((($num_days - $cur_days) / $num_days) * $budgets['main']['budget']); // sisa seharusnya
                $usage_main['value'] = $budgets['main']['budget'] - $budgets['main']['value']; // sisa
                $usage_main['name'] = ($usage_main['value'] < $usage_main['budget'] ? 'Boros' : ($usage_main['value'] > $usage_main['budget'] ? 'Hemat' : ''));
                $usage_main['per_day'] = ($usage_main['value'] <= 0) ? 0 : ($usage_main['value'] / (Carbon::now()->diffInDays(Carbon::now()->endOfMonth()) == 0 ? 1 : Carbon::now()->diffInDays(Carbon::now()->endOfMonth())));

                $total['percentage'] = floor(($total['budget'] - $total['value'] > 0) ? ($total['value'] / $total['budget'] * 100) : 100);
                $total['color'] = $color[floor($total['percentage'] / 25)];

                return [
                    'cash_in_hand' => $cashInHand,
                    'budget' => $budgets,
                    'targets' => $targets,
                    'usage' => $usage,
                    'usage_main' => $usage_main,
                    'total' => $total
                ];
            }
        } catch (Exception $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }
}
