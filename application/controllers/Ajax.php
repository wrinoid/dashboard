<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use Carbon\Carbon;

class Ajax extends MY_Controller
{
    private $new_hash;

    public function __construct()
    {
        parent::__construct();

        $this->new_hash = $this->security->get_csrf_hash();

        if (!$this->authentication()) {
            return response(401);
        }

        $this->load->model('M_system');
    }

    public function updateProfile()
    {
        try {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'Full Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
            $this->form_validation->set_rules('username', 'Username', 'required|callback_username_check');

            if ($this->form_validation->run() == false) {
                return $this->response(
                    400, [
                    'message' => 'Gagal validasi',
                    'validation_errors' => $this->form_validation->error_array()
                    ]
                );
            } else {
                $this->M_system->update('t_users', ['user_id' => $this->auth->user_id], ['name' => $this->request['name'], 'username' => $this->request['username'], 'email' => $this->request['email']]);

                $this->authentication(); // re-auth to update cookie data

                return $this->response(
                    200, [
                    'message' => 'Profile berhasil diubah'
                    ]
                );
            }
        } catch (Exception $e) {
            return $this->response(
                500, [
                'message' => $e->getMessage()
                ]
            );
        }
    }

    public function email_check($email)
    {
        $exist = $this->M_system->count('t_users', ['email' => $email, 'user_id !=' => $this->auth->user_id]);

        if ($exist > 0) {
            $this->form_validation->set_message('email_check', 'Email has already taken.');
            return false;
        } else {
            return true;
        }
    }

    public function username_check($username)
    {
        $exist = $this->M_system->count('t_users', ['username' => $username, 'user_id !=' => $this->auth->user_id]);

        if ($exist > 0) {
            $this->form_validation->set_message('username_check', 'Username has already taken.');
            return false;
        } else {
            return true;
        }
    }

    public function updatePassword()
    {
        try {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('old_password', 'Old Password', 'required|callback_old_pass_check');
            $this->form_validation->set_rules('new_password', 'New Password', 'required');
            $this->form_validation->set_rules('confirm_password', 'Confrim Password', 'required|matches[new_password]');

            if ($this->form_validation->run() == false) {
                return $this->response(
                    400, [
                    'message' => 'Validation fail.',
                    'validation_errors' => $this->form_validation->error_array()
                    ]
                );
            } else {
                $this->M_system->update('t_users', ['user_id' => $this->auth->user_id], ['password' => password_hash($this->request['new_password'], PASSWORD_BCRYPT)]);

                $this->authentication(); // re-auth to update cookie data

                return $this->response(
                    200, [
                    'message' => 'Password berhasil diubah.'
                    ]
                );
            }
        } catch (Exception $e) {
            return $this->response(
                500, [
                'message' => $e->getMessage()
                ]
            );
        }
    }

    public function old_pass_check($old_pass)
    {
        $user = $this->M_system->get('t_users', ['user_id' => $this->auth->user_id])->row();

        if (!password_verify($old_pass, $user->password)) {
            $this->form_validation->set_message('old_pass_check', 'Wrong old password.');
            return false;
        } else {
            return true;
        }
    }

    public function updatePhoto()
    {
        try {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('image', 'Photo', 'required');
            
            if ($this->form_validation->run() == false) {
                return $this->response(
                    400, [
                    'message' => 'Gagal validasi',
                    'validation_errors' => $this->form_validation->error_array()
                    ]
                );
            } else {
                if (!file_exists(FCPATH.'assets/img/profile/')) {
                    mkdir(FCPATH.'assets/img/profile/', 0755, true);
                }

                $data = $this->request['image'];
                $image_array_1 = explode(';', $data);
                $image_array_2 = explode(',', $image_array_1[1]);
                $data = base64_decode($image_array_2[1]);

                $fileName = $this->auth->username.'-'.time().'.png';
                $fullPath = FCPATH.'assets/img/profile/'.$fileName;
                file_put_contents($fullPath, $data);

                delete_files(FCPATH.'assets/img/profile/'.$this->auth->photo);

                $this->M_system->update('t_users', ['user_id' => $this->auth->user_id], ['photo' => $fileName]);

                $this->authentication(); // re-auth to update cookie data
                
                return $this->response(
                    200, [
                    'message' => 'Photo berhasil diubah.',
                    'data' => base_url('assets/img/profile/'.$fileName)
                    ]
                );
            }
        } catch (Exception $e) {
            return $this->response(
                500, [
                'message' => $e->getMessage()
                ]
            );
        }
    }

    public function datatables()
    {
        if ($this->request['type']) {
            $type = $this->request['type'];

            if ($type == 'get-detail-location') {
                $json = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $this->request['latitude'] . ',' . $this->request['longitude'] . '&sensor=false&key=AIzaSyB9AlI18Nx9cc6yLlYMNOLQpYWWNJqLiUo');

                return $this->response(200, ['map' => json_decode($json)]);
            } elseif ($type == 'get-list-visitor') {
                $columns = array(
                    0 => 'timestamp',
                    1 => 'ip_address',
                    2 => 'coordinate',
                    3 => 'referer',
                    4 => 'browser',
                    5 => 'finished',
                );

                $totalData = $this->M_system->count('t_visitors');

                $totalFiltered = $totalData;

                $limit = $this->request['length'];
                $start = $this->request['start'];
                $order = $columns[$this->request['order'][0]['column']];
                $dir = $this->request['order'][0]['dir'];

                if (empty($this->request['search']['value'])) {
                    $rows = $this->M_system->get_visitor_datatable($limit, $start, $order, $dir);
                } else {
                    $search = $this->request['search']['value'];

                    $where = "timestamp like '%" . $search . "%' OR ip_address like '%" . $search . "%' OR browser like '%" . $search . "%' OR referer like '%" . $search . "%'";

                    $rows = $this->M_system->get_visitor_datatable($limit, $start, $order, $dir, $where);

                    $totalFiltered = $this->M_system->count('t_visitors', $where);
                }

                $data = array();
                foreach ($rows as $row) {
                    $nestedData['timestamp'] = Carbon::create($row->timestamp)->format('Y/m/d H:i:s'); // date_format(date_create($row->timestamp), 'Y/m/d H:i:s');
                    $nestedData['ip_address'] = "<a href='#' data-toggle='modal' data-target='#modal-detail' data-tipe='ip' data-ip='" . $row->ip_address . "'>" . $row->ip_address . "</a>";
                    if ($row->latitude != null and $row->longitude != null) {
                        $nestedData['coordinate'] = "<a href='#' data-toggle='modal' data-target='#modal-detail' data-tipe='coordinate' data-latitude='" . $row->latitude . "' data-longitude='" . $row->longitude . "'>Lihat Detail</a> | <a href='https://www.google.com/maps/?q=" . $row->latitude . "," . $row->longitude . "' target='_blank'>Open in maps</a>";
                    } else {
                        $nestedData['coordinate'] = "-";
                    }
                    $nestedData['browser'] = $row->browser;
                    $nestedData['referer'] = ($row->referer != null or $row->referer != "") ? $row->referer : '-';
                    $nestedData['finished'] = ($row->finished == 0) ? '<span class="label label-danger">No</span>' : '<span class="label label-success">Yes</span>';

                    $data[] = $nestedData;
                }

                $json_data = array(
                    "draw"            => intval($this->request['draw']),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                );

                return $this->response(200, $json_data);
            } elseif ($_POST['type'] == 'get-ip-detail') {
                $ch = curl_init('ipinfo.io/' . $_POST['ip'] . '?token=87fb8bf004b265');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                curl_close($ch);

                $response = json_decode($response);
                $json = "<table class='table table-hover table-striped'>";
                foreach ($response as $key => $value) {
                    $json .= "<tr>";
                    $json .= "<th>" . $key . "</th>";
                    $json .= "<td>:</td>";
                    if ($key == "loc") {
                        $map = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $value . '&sensor=false&key=AIzaSyB9AlI18Nx9cc6yLlYMNOLQpYWWNJqLiUo'));
                        $coordinate = explode(",", $value);
                        $json .= "<td>" . $map->results[0]->formatted_address . " | <a href='https://www.google.com/maps/?q=" . $value . "' target='_blank'>Open in maps</a></td>";
                    } else {
                        $json .= "<td>" . $value . "</td>";
                    }
                    $json .= "</tr>";
                }
                $json .= "</table>";

                return $this->response(200, ['html' => $json]);
            }
        }
    }

    public function listAccount()
    {
        $this->load->model('M_account');

        $columns = array(
            0 => 'account_id',
            1 => 'account_name',
            2 => 'balance',
            3 => 'action'
        );

        $totalData = $this->M_account->count();

        $totalFiltered = $totalData;

        $limit = $this->request['length'];
        $start = $this->request['start'];
        $order = $columns[$this->request['order'][0]['column']];
        $dir = $this->request['order'][0]['dir'];

        if (empty($this->request['search']['value'])) {
            $rows = $this->M_account->get_datatable($limit, $start, $order, $dir);
        } else {
            $search = $this->request['search']['value'];

            $where = "account_id like '%" . $search . "%' OR account_name like '%" . $search . "%' OR balance like '%" . $search . "%'";

            $rows = $this->M_account->get_datatable($limit, $start, $order, $dir, $where);

            $totalFiltered = $this->M_account->count($where);
        }

        $data = array();
        foreach ($rows as $row) {
            $nestedData['account_id'] = $row->account_id;
            $nestedData['account_name'] = $row->account_name;
            $nestedData['balance'] = 'Rp. '.number_format($row->balance, 2);
            $nestedData['action'] = '';

            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"            => intval($this->request['draw']),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return $this->response(200, $json_data);
    }

    public function addAccount()
    {
        try {
            $this->load->model('M_account');

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name_add_account', 'Account Name', 'required|callback_account_name_check');

            if ($this->form_validation->run() == false) {
                return $this->response(
                    400, [
                    'message' => 'Gagal validasi',
                    'validation_errors' => $this->form_validation->error_array()
                    ]
                );
            } else {
                $this->M_account->insert(['account_name' => $this->request['name_add_account']]);

                return $this->response(
                    200, [
                    'message' => 'Akun berhasil ditambahkan'
                    ]
                );
            }
        } catch (Exception $e) {
            return $this->response(
                500, [
                'message' => $e->getMessage()
                ]
            );
        }
    }

    public function account_name_check($name)
    {
        $exist = $this->M_account->count(['account_name' => $name]);

        if ($exist > 0) {
            $this->form_validation->set_message('account_name_check', 'Account name has already taken.');
            return false;
        } else {
            return true;
        }
    }

    public function listCategory()
    {
        $this->load->model('M_category');

        $columns = array(
            0 => 'category_id',
            1 => 'category_name',
            2 => 'type',
            3 => 'action'
        );

        $totalData = $this->M_category->count();

        $totalFiltered = $totalData;

        $limit = $this->request['length'];
        $start = $this->request['start'];
        $order = $columns[$this->request['order'][0]['column']];
        $dir = $this->request['order'][0]['dir'];

        if (empty($this->request['search']['value'])) {
            $rows = $this->M_category->get_datatable($limit, $start, $order, $dir);
        } else {
            $search = $this->request['search']['value'];

            $where = "category_id like '%" . $search . "%' OR category_name like '%" . $search . "%'";

            $rows = $this->M_category->get_datatable($limit, $start, $order, $dir, $where);

            $totalFiltered = $this->M_category->count($where);
        }

        $data = array();
        foreach ($rows as $row) {
            $nestedData['category_id'] = $row->category_id;
            $nestedData['category_name'] = $row->category_name;
            $nestedData['type'] = $row->type;
            $nestedData['action'] = '';

            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"            => intval($this->request['draw']),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return $this->response(200, $json_data);
    }

    public function addCategory()
    {
        try {
            $this->load->model('M_category');

            $this->load->library('form_validation');

            $this->form_validation->set_rules('name_add_category', 'Category Name', 'required|callback_category_name_check['.$this->request['type_add_category'].']');
            $this->form_validation->set_rules('type_add_category', 'Type', 'required|in_list[Debit,Credit]');

            if ($this->form_validation->run() == false) {
                return $this->response(
                    400, [
                    'message' => 'Gagal validasi',
                    'validation_errors' => $this->form_validation->error_array()
                    ]
                );
            } else {
                $this->M_category->insert(['category_name' => $this->request['name_add_category'], 'type' => $this->request['type_add_category']]);

                return $this->response(
                    200, [
                    'message' => 'Kategori berhasil ditambahkan'
                    ]
                );
            }
        } catch (Exception $e) {
            return $this->response(
                500, [
                'message' => $e->getMessage()
                ]
            );
        }
    }

    public function category_name_check($name, $type)
    {
        $exist = $this->M_category->count(['category_name' => $name, 'type' => $type]);

        if ($exist > 0) {
            $this->form_validation->set_message('category_name_check', 'Category name has already taken.');
            return false;
        } else {
            return true;
        }
    }

    public function getCategory()
    {
        try {
            $this->load->model('M_category');

            $this->load->library('form_validation');

            $this->form_validation->set_rules('type_add_journal', 'Type', 'required|in_list[Debit,Credit]');

            if ($this->form_validation->run() == false) {
                return $this->response(
                    400, [
                    'message' => 'Gagal validasi',
                    'validation_errors' => $this->form_validation->error_array()
                    ]
                );
            } else {
                $categories = $this->M_category->get(['type' => $this->request['type_add_journal']]);

                return $this->response(
                    200, [
                    'message' => 'Sukses ambil data',
                    'data' => $categories->result()
                    ]
                );
            }
        } catch (Exception $e) {
            return $this->response(
                500, [
                'message' => $e->getMessage()
                ]
            );
        }
    }

    public function listJournal()
    {
        $this->load->model('M_journal');

        $columns = array(
            0 => 'created_at',
            1 => 'account_name',
            2 => 'debit',
            3 => 'credit',
            4 => 'category_name',
            5 => 'reference',
            6 => 'action'
        );

        $totalData = $this->M_journal->count();

        $totalFiltered = $totalData;

        $limit = $this->request['length'];
        $start = $this->request['start'];
        $order = $columns[$this->request['order'][0]['column']];
        $dir = $this->request['order'][0]['dir'];

        if (empty($this->request['search']['value'])) {
            $rows = $this->M_journal->get_datatable($limit, $start, $order, $dir);
        } else {
            $search = $this->request['search']['value'];

            $where = "reference like '%" . $search . "%'";

            $rows = $this->M_journal->get_datatable($limit, $start, $order, $dir, $where);

            $totalFiltered = $this->M_journal->count($where);
        }

        $data = array();
        foreach ($rows as $row) {
            $nestedData['created_at'] = Carbon::create($row->created_at)->format('Y/m/d H:i:s');
            $nestedData['account_name'] = $row->account_name;
            $nestedData['debit'] = ($row->type == 'Debit') ? 'Rp. '.number_format($row->amount, 2) : '';
            $nestedData['credit'] = ($row->type == 'Credit') ? 'Rp. '.number_format($row->amount, 2) : '';
            $nestedData['category_name'] = $row->category_name;
            $nestedData['reference'] = $row->reference;
            $nestedData['action'] = '';

            $data[] = $nestedData;
        }

        $json_data = array(
            "draw"            => intval($this->request['draw']),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return $this->response(200, $json_data);
    }

    public function addJournal()
    {
        try {
            $this->load->model('M_account');
            $this->load->model('M_journal');

            $this->load->library('form_validation');

            $this->form_validation->set_rules('type_add_journal', 'Type', 'required|in_list[Debit,Credit,Transfer]');
            $this->form_validation->set_rules('account_from_add_journal', 'Account From', 'required');
            $this->form_validation->set_rules('account_to_add_journal', 'Account To', 'differs[account_from_add_journal]');
            if (!empty($this->request['account_from_add_journal']) && $this->request['type_add_journal'] == 'Transfer') {
                $this->form_validation->set_rules('account_to_add_journal', 'Account To', 'required');
            }
            $this->form_validation->set_rules('amount_add_journal', 'Amount', 'required|numeric');
            if ($this->request['type_add_journal'] != 'Transfer') {
                $this->form_validation->set_rules('category_add_journal', 'Category', 'required');
            }

            if ($this->form_validation->run() == false) {
                return $this->response(
                    400, [
                    'message' => 'Gagal validasi',
                    'validation_errors' => $this->form_validation->error_array()
                    ]
                );
            } else {
                $this->db->trans_begin();

                if ($this->request['type_add_journal'] != 'Transfer') {
                    $this->M_journal->insert(['type' => $this->request['type_add_journal'], 'amount' => $this->request['amount_add_journal'], 'account_id' => $this->request['account_from_add_journal'], 'category_id' => $this->request['category_add_journal'], 'reference' => $this->request['reference_add_journal']]);
                    $account_from = $this->M_account->get(['account_id' => $this->request['account_from_add_journal']])->row();

                    $balance = $account_from->balance - $this->request['amount_add_journal'];
                    if ($this->request['type_add_journal'] == 'Credit') {
                        $balance = $account_from->balance + $this->request['amount_add_journal'];
                    }

                    $this->M_account->update(['account_id' => $account_from->account_id], ['balance' => $balance]);

                } else {
                    $this->M_journal->insert(['type' => 'Debit', 'amount' => $this->request['amount_add_journal'], 'account_id' => $this->request['account_from_add_journal'], 'reference' => $this->request['reference_add_journal']]);
                    $account_from = $this->M_account->get(['account_id' => $this->request['account_from_add_journal']])->row();
                    $this->M_account->update(['account_id' => $account_from->account_id], ['balance' => $account_from->balance - $this->request['amount_add_journal']]);

                    $this->M_journal->insert(['type' => 'Credit', 'amount' => $this->request['amount_add_journal'], 'account_id' => $this->request['account_to_add_journal'], 'reference' => $this->request['reference_add_journal']]);
                    $account_to = $this->M_account->get(['account_id' => $this->request['account_to_add_journal']])->row();
                    $this->M_account->update(['account_id' => $account_to->account_id], ['balance' => $account_to->balance + $this->request['amount_add_journal']]);
                }

                $this->db->trans_commit();

                return $this->response(
                    200, [
                    'message' => 'Akun berhasil ditambahkan'
                    ]
                );
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();

            return $this->response(
                500, [
                'message' => $e->getMessage()
                ]
            );
        }
    }

    private function response($status_code, $data)
    {
        $data['new_hash'] = $this->new_hash;

        return response($status_code, $data);
    }
}
