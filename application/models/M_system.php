<?php

class M_system extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /* GET DATA */
    public function get($table, $where = [])
    {
        $query = $this->db->get_where($table, $where);
        return $query;
    }

    /* COUNT DATA */
    public function count($table, $where = "")
    {
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->from($table);
        $query = $this->db->count_all_results();
        return $query;
    }

    /* INSERT */
    public function insert($table, $data)
    {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    /* INSERT BATCH */
    public function insert_batch($table, $data)
    {
        $this->db->insert_batch($table, $data);
    }

    /* UPDATE */
    public function update($table, $where, $data)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    /* DELETE */
    public function remove($table, $where)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    public function get_visitor_datatable($limit, $start, $order, $dir, $where = "")
    {
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->from('t_visitors');
        $this->db->limit($limit, $start);
        $this->db->order_by($order, $dir);
        $query = $this->db->get();

        return $query->result();
    }
}
