<?php

use Carbon\Carbon;

class M_account extends CI_Model
{
    private $db;

    public function __construct()
    {
        parent::__construct();

        $this->db = $this->load->database('accounting', TRUE);
    }

    /* GET DATA */
    public function get($where = [])
    {
        $this->db->where($where);
        $this->db->where('deleted_at IS NULL');
        $query = $this->db->get('t_account');
        return $query;
    }

    /* COUNT DATA */
    public function count($where = "")
    {
        $this->db->where('deleted_at IS NULL');
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->from('t_account');
        $query = $this->db->count_all_results();
        return $query;
    }

    /* INSERT */
    public function insert($data)
    {
        $this->db->insert('t_account', $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    /* INSERT BATCH */
    public function insert_batch($data)
    {
        $this->db->insert_batch('t_account', $data);
    }

    /* UPDATE */
    public function update($where, $data)
    {
        $this->db->where($where);
        $this->db->update('t_account', $data);
    }

    /* DELETE */
    public function remove($where)
    {
        $this->db->where($where);
        $this->db->update('t_account', ['deleted_at', Carbon::now()]);
    }

    public function get_datatable($limit, $start, $order, $dir, $where = "")
    {
        $this->db->where('deleted_at IS NULL');

        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->from('t_account');
        $this->db->limit($limit, $start);
        $this->db->order_by($order, $dir);
        $query = $this->db->get();

        return $query->result();
    }

    public function getSaldo()
    {
        $this->db->where('deleted_at IS NULL');
        $this->db->select_sum('balance');
        $query = $this->db->get('t_account');

        return $query->row();
    }
}
