<?php

use Carbon\Carbon;

class M_category extends CI_Model
{
    private $db;

    public function __construct()
    {
        parent::__construct();

        $this->db = $this->load->database('accounting', TRUE);
    }

    /* GET DATA */
    public function get($where = [])
    {
        $this->db->where($where);
        $this->db->where('deleted_at IS NULL');
        $query = $this->db->get('t_category');
        return $query;
    }

    /* COUNT DATA */
    public function count($where = "")
    {
        $this->db->where('deleted_at IS NULL');
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->from('t_category');
        $query = $this->db->count_all_results();
        return $query;
    }

    /* INSERT */
    public function insert($data)
    {
        $this->db->insert('t_category', $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    /* INSERT BATCH */
    public function insert_batch($data)
    {
        $this->db->insert_batch('t_category', $data);
    }

    /* UPDATE */
    public function update($where, $data)
    {
        $this->db->where($where);
        $this->db->update('t_category', $data);
    }

    /* DELETE */
    public function remove($where)
    {
        $this->db->where($where);
        $this->db->update('t_category', ['deleted_at', Carbon::now()]);
    }

    public function get_datatable($limit, $start, $order, $dir, $where = "")
    {
        $this->db->where('deleted_at IS NULL');

        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->from('t_category');
        $this->db->limit($limit, $start);
        $this->db->order_by($order, $dir);
        $query = $this->db->get();

        return $query->result();
    }
}
