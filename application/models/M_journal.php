<?php

use Carbon\Carbon;

class M_journal extends CI_Model
{
    private $db;

    public function __construct()
    {
        parent::__construct();

        $this->db = $this->load->database('accounting', TRUE);
    }

    /* GET DATA */
    public function get($where = [])
    {
        $this->db->where($where);
        $this->db->where('deleted_at IS NULL');
        $query = $this->db->get('t_journal');
        return $query;
    }

    /* COUNT DATA */
    public function count($where = "")
    {
        $this->db->where('deleted_at IS NULL');
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->from('t_journal');
        $query = $this->db->count_all_results();
        return $query;
    }

    /* INSERT */
    public function insert($data)
    {
        $this->db->insert('t_journal', $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    /* INSERT BATCH */
    public function insert_batch($data)
    {
        $this->db->insert_batch('t_journal', $data);
    }

    /* UPDATE */
    public function update($where, $data)
    {
        $this->db->where($where);
        $this->db->update('t_journal', $data);
    }

    /* DELETE */
    public function remove($where)
    {
        $this->db->where($where);
        $this->db->update('t_journal', ['deleted_at', Carbon::now()]);
    }

    public function get_datatable($limit, $start, $order, $dir, $where = "")
    {
        $this->db->where('t_journal.deleted_at IS NULL');

        $this->db->select('t_journal.*, t_account.account_name, t_category.category_name');
        if ($where != "") {
            $this->db->where($where);
        }
        $this->db->from('t_journal');
        $this->db->join('t_account', 't_account.account_id = t_journal.account_id');
        $this->db->join('t_category', 't_category.category_id = t_journal.category_id', 'left');
        $this->db->limit($limit, $start);
        $this->db->order_by('t_journal.'.$order, $dir);
        $query = $this->db->get();

        return $query->result();
    }

    public function getModal()
    {
        $this->db->where(['category_name' => 'Modal', 'type' => 'Credit']);
        $this->db->where('deleted_at IS NULL');
        $category = $this->db->get('t_category')->row();

        $this->db->where('category_id', $category->category_id);
        $this->db->where('deleted_at IS NULL');
        $this->db->select_sum('amount');
        $query = $this->db->get('t_journal');

        return $query->row();
    }

    public function getLineChart($month, $year)
    {

        $this->db->where(['category_name' => 'Sales', 'type' => 'Credit']);
        $this->db->where('deleted_at IS NULL');
        $category = $this->db->get('t_category')->row();

        $query = $this->db->query("
            SELECT
                DAY(created_at) AS tanggal,
                SUM(amount) AS amount
            FROM t_journal
            WHERE
                MONTH(created_at) = $month AND
                YEAR(created_at) = $year AND
                category_id = $category->category_id AND
                deleted_at IS NULL
            GROUP BY tanggal
        ");

        return $query->result();
    }
}
