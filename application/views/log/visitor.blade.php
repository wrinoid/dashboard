@extends('layouts.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<style>
  .vertical-middle {
    vertical-align: middle !important;
  }
  .dtr-data {
    word-wrap: anywhere;
  }
</style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Log Visitor</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Log Visitor</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header with-border">
              <h3 class="card-title">Log Visitor</h3>
            </div>
            <div class="card-body">
              <table id="table-visitor" class="table table-striped table-bordered dt-responsive">
                <thead>
                  <tr>
                    <th>Timestamp</th>
                    <th>IP Address</th>
                    <th>Coordinate</th>
                    <th>Referer</th>
                    <th>Browser</th>
                    <th>Finished</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="text-center"><i class="fas fa-3x fa-sync-alt fa-spin"></i></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{ base_url('assets/plugins/v3.2/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/jszip/jszip.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script type="text/javascript" charset="utf8" src="{{ base_url('assets/plugins/datatables/extensions/Input/input.js') }}"></script>


<script type="text/javascript">
  $(document).ready(function(){
    var table = $('#table-visitor').DataTable({
      'processing': true,
      'serverSide': true,
      'responsive': {
        'details': {
          'type': 'column'
        }
      },
      'ajax': {
        'url': '{{ site_url("ajax/datatables") }}',
        'dataType': 'json',
        'type': 'POST',
        'data': function(d){
          d.type = 'get-list-visitor',
          d.csrf_test_name = $('#csrf').val()
        }
      },
      'columns': [
      {
        'data': 'timestamp',
        'className': 'vertical-middle'
      },
      {
        'data': 'ip_address',
        'className': 'vertical-middle'
      },
      {
        'data': 'coordinate',
        'className': 'vertical-middle'
      },
      {
        'data': 'referer',
        'className': 'vertical-middle'
      },
      {
        'data': 'browser',
        'className': 'vertical-middle'
      },
      {
        'data': 'finished',
        'className': 'vertical-middle text-center'
      }
      ],
      'order': [ 0, 'desc' ],
      // 'pagingType': 'input',
      'drawCallback': function(settings){
        $('.csrf').val(settings.json.new_hash);
      }
    });

    $('#modal-detail').on('show.bs.modal', function(e) {
      var tipe = $(e.relatedTarget).data('tipe');
      if(tipe == 'ip'){
        var ip = $(e.relatedTarget).data('ip');
        $(e.currentTarget).find('.modal-body').html(`<div class="text-center"><i class="fas fa-3x fa-sync-alt fa-spin"></i></div>`);
        $.ajax({
          type: 'POST',
          url: '{{ site_url("ajax/datatables") }}',
          dataType: 'JSON',
          data: { csrf_test_name: $('#csrf').val(), type: 'get-ip-detail', ip: ip },
          success: function(datas){
            $('.csrf').val(datas.new_hash);
            $(e.currentTarget).find('.modal-body').html(datas.html);
          },
          error: function(datas){
            console.log(datas);
          }
        });
      }
      else if(tipe == 'coordinate'){
        var lat = $(e.relatedTarget).data('latitude');
        var lng = $(e.relatedTarget).data('longitude');
        $(e.currentTarget).find('.modal-body').html(`<div class="text-center"><i class="fas fa-3x fa-sync-alt fa-spin"></i></div>`);
        $.ajax({
          type: 'POST',
          url: '{{ site_url("ajax/datatables") }}',
          dataType: 'JSON',
          data: { csrf_test_name: $('#csrf').val(), type: 'get-detail-location', latitude: lat, longitude: lng },
          success: function(datas){
            $('.csrf').val(datas.new_hash);
            $(e.currentTarget).find('.modal-body').html(datas.map.results[0].formatted_address);
          },
          error: function(datas){
            console.log(datas);
          }
        });
      }
    });
  });
</script>
@endsection