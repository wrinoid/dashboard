@extends('layouts.main')

@section('css')
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/croppie/croppie.css') }}">
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">
          <div class="card card-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-navy">
              <h3 class="widget-user-username">{{ $CI->auth->name }}</h3>
              <h5 class="widget-user-desc">{{ $CI->auth->email }}</h5>
            </div>
            <div class="widget-user-image" id="profile-photo">
              <div class="overlay">
                <i class="fas fa-camera text-white"></i>
              </div>
              <img class="img-circle elevation-2 profile-photo" src="{{ base_url('assets/img/profile/'.$CI->auth->photo) }}" alt="User Avatar">
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-12">
                  <div class="description-block">
                    <h5 class="description-header">{{ Carbon\Carbon::create('1994-01-08')->longAbsoluteDiffForHumans() }}</h5>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <input type="file" name="upload_image" id="upload_image" accept="image/*" class="d-none" />
          </div>
        </div>
        <div class="col-md-9">
          <div class="card">
            <div class="card-header with-border">
              <h3 class="card-title">Profile Data</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="name">Full Name</label>
                <input type="text" class="form-control" id="name" placeholder="Enter full name" value="{{ $CI->auth->name }}">
              </div>
              <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" id="email" placeholder="Enter email" value="{{ $CI->auth->email }}">
              </div>
              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" placeholder="Enter usename" value="{{ $CI->auth->username }}">
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="update-profile">Update Profile</button>
            </div>
          </div>
          <div class="card">
            <div class="card-header with-border">
              <h3 class="card-title">Password</h3>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="old-password">Old Password</label>
                <input type="password" class="form-control" id="old_password" placeholder="Enter old password">
              </div>
              <div class="form-group">
                <label for="new_password">New Password</label>
                <input type="password" class="form-control" id="new_password" placeholder="Enter new password">
              </div>
              <div class="form-group">
                <label for="confirm_password">Confirm Password</label>
                <input type="password" class="form-control" id="confirm_password" placeholder="Enter confirm password">
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="update-password">Update Password</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="crop-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div id="cropper"></div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="cancel-photo">Close</button>
        <button type="button" class="btn btn-primary" id="update-photo">Upload Photo</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('script')
<script src="{{ base_url('assets/plugins/v3.2/croppie/croppie.min.js') }}"></script>
<script>
  $(document).ready(function(){
    $('#update-profile').click(function(e) {
      var el = this;
      $(el).addClass('disabled').prop('disabled', true).html('<div class="spinner-border spinner-border-sm mx-3" role="status"><span class="sr-only">Loading...</span></div>')

      clearAllValidation()

      $.ajax({
        type: 'POST',
        url: '{{ site_url("ajax/update-profile") }}',
        dataType: 'JSON',
        data: { csrf_test_name: $('#csrf').val(), name: $('#name').val(), email: $('#email').val(), username: $('#username').val() },
        success: function(response){
          $('.csrf').val(response.new_hash)
          toast('bg-success', 'Sukses', '', response.message)
          $(el).removeClass('disabled').prop('disabled', false).text('Update Profile')
        },
        error: function(response, textStatus, errorThrown){
          $('.csrf').val(response.responseJSON.new_hash)
          toast('bg-danger', 'Oops', response.status, response.responseJSON.message)

          if(response.status == 400){
            parseValidation(response.responseJSON.validation_errors)
          }
          $(el).removeClass('disabled').prop('disabled', false).text('Update Profile')
        }
      });
    });

    $('#update-password').click(function(e) {
      var el = this;
      $(el).addClass('disabled').prop('disabled', true).html('<div class="spinner-border spinner-border-sm mx-3" role="status"><span class="sr-only">Loading...</span></div>')

      clearAllValidation()

      $.ajax({
        type: 'POST',
        url: '{{ site_url("ajax/update-password") }}',
        dataType: 'JSON',
        data: { csrf_test_name: $('#csrf').val(), old_password: $('#old_password').val(), new_password: $('#new_password').val(), confirm_password: $('#confirm_password').val() },
        success: function(response){
          $('.csrf').val(response.new_hash)
          toast('bg-success', 'Sukses', '', response.message)
          $(el).removeClass('disabled').prop('disabled', false).text('Update Password')
        },
        error: function(response, textStatus, errorThrown){
          $('.csrf').val(response.responseJSON.new_hash)
          toast('bg-danger', 'Oops', response.status, response.responseJSON.message)

          if(response.status == 400){
            parseValidation(response.responseJSON.validation_errors)
          }
          $(el).removeClass('disabled').prop('disabled', false).text('Update Password')
        }
      });
    });

    $('#profile-photo').click(function(e) {
      $('#upload_image').trigger('click'); 
    });

    var $uploadCrop,
    tempFilename,
    rawImg,
    imageId;

    function readFile(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
          $('#cropper').addClass('ready');
          $('#crop-modal').modal('show');
          rawImg = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
      }
      else {
        // swal("Sorry - you're browser doesn't support the FileReader API");
      }
    }

    $uploadCrop = $('#cropper').croppie({
      enableExif: true,
      enforceBoundary: true,
      viewport: {
        width: 200,
        height: 200,
        type:'circle',
      },
      boundary:{
        width:300,
        height:300
      }
    });

    $('#crop-modal').on('shown.bs.modal', function(){
        // alert('Shown pop');
        $uploadCrop.croppie('bind', {
          url: rawImg
        }).then(function(){
          console.log('jQuery bind complete');
        });
      });

    $('#upload_image').on('change', function () {
      imageId = $(this).data('id'); tempFilename = $(this).val();
      $('#cancel-photo').data('id', imageId); readFile(this);
    });

    $('#update-photo').on('click', function (e) {
      var el = this;
      $(el).addClass('disabled').prop('disabled', true).html('<div class="spinner-border spinner-border-sm mx-3" role="status"><span class="sr-only">Loading...</span></div>')

      clearAllValidation()

      $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function(dataImage){
        $.ajax({
          type: 'POST',
          url: '{{ site_url("ajax/update-photo") }}',
          data: { csrf_test_name: $('#csrf').val(), 'image': dataImage },
          success:function(response){
            $('.csrf').val(response.new_hash)
            $('.profile-photo').attr('src', response.data)
            toast('bg-success', 'Sukses', '', response.message)
            $(el).removeClass('disabled').prop('disabled', false).text('Upload Photo')

            $('#crop-modal').modal('hide');
          },
          error: function(response, textStatus, errorThrown){
            $('.csrf').val(response.responseJSON.new_hash)
            toast('bg-danger', 'Oops', response.status, response.responseJSON.message)

            if(response.status == 400){
              parseValidation(response.responseJSON.validation_errors)
            }
            $(el).removeClass('disabled').prop('disabled', false).text('Update Password')
          }
        });
      })
    });
  });
</script>
@endsection