@extends('layouts.main')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        
      </div>
    </div>
    <!--/. container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')
<!-- jQuery Knob -->
<script src="{{ base_url('assets/plugins/v3.2/jquery-knob/jquery.knob.min.js') }}"></script>
<script>
  $(function () {
    $('.knob').knob({
      'format' : function (value) {
        return value + '%';
      }
    })

    $('[data-toggle="tooltip"]').tooltip()
  })
</script>
<script>
  window.Userback = window.Userback || {};
  Userback.access_token = 'A-Avja6brZ3IP4Wq9QTRy94Uttq';
  (function(d) {var s = d.createElement('script');s.async = true;s.src = 'https://static.userback.io/widget/v1.js';(d.head || d.body).appendChild(s);})(document);
</script>
<script>
  window.intercomSettings = {
    api_base: "https://api-iam.intercom.io",
    app_id: "q1c9awgm",
    user_id: 1,
    name: 'Rino',
    email: 'winapamungkasr@gmail.com',
    created_at: 1704067200,
  };
</script>
<script>
        // We pre-filled your app ID in the widget URL: 'https://widget.intercom.io/widget/q1c9awgm'
  (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/q1c9awgm';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(document.readyState==='complete'){l();}else if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
</script>
@endsection