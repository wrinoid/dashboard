@extends('layouts.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<style>
  .vertical-middle {
    vertical-align: middle !important;
  }
  .dtr-data {
    word-wrap: anywhere;
  }
</style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Accounting</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Categories</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header with-border">
              <h3 class="card-title">Categories</h3>
              <div class="card-tools">
                <ul class="nav nav-pills ml-auto">
                  <li class="nav-item">
                    <button class="btn btn-sm btn-primary" id="btn-add-category"><i class="fas fa-plus"></i> Add Category</button>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-body">
              <table id="table-categories" class="table table-striped table-bordered dt-responsive">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Category Name</th>
                    <th>Type</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/. container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="modal-add-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Category</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-add-category">
          <div class="form-group">
            <label for="name_add_category">Category Name</label>
            <input type="text" class="form-control" id="name_add_category" placeholder="Enter category name">
          </div>
          <div class="form-group">
            <label for="type_add_category">Type</label>
            <select class="form-control" id="type_add_category">
              <option value="Debit">Debit</option>
              <option value="Credit">Credit</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-add-category">Save</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{ base_url('assets/plugins/v3.2/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/jszip/jszip.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script type="text/javascript" charset="utf8" src="{{ base_url('assets/plugins/datatables/extensions/Input/input.js') }}"></script>


<script type="text/javascript">
  $(document).ready(function(){
    var table = $('#table-categories').DataTable({
      'processing': true,
      'serverSide': true,
      'responsive': {
        'details': {
          'type': 'column'
        }
      },
      'ajax': {
        'url': '{{ site_url("ajax/list-category") }}',
        'dataType': 'json',
        'type': 'POST',
        'data': function(d){
          d.csrf_test_name = $('#csrf').val()
        }
      },
      'columns': [
        {
          'data': 'category_id',
          'className': 'vertical-middle'
        },
        {
          'data': 'category_name',
          'className': 'vertical-middle'
        },
        {
          'data': 'type',
          'className': 'vertical-middle text-center'
        },
        {
          'data': 'action',
          'className': 'vertical-middle text-center',
          'orderable': false
        }
      ],
      'order': [ 0, 'desc' ],
      // 'pagingType': 'input',
      'drawCallback': function(settings){
        $('.csrf').val(settings.json.new_hash);
      }
    });

    $('#table-categories thead th').removeClass('text-center').removeClass('text-right');

    $('#btn-add-category').click(function(e){
      clearAllValidation()
      $('#modal-add-category').modal('show');
    });

    $('#save-add-category').click(function(e){
      var el = this;
      $(el).addClass('disabled').prop('disabled', true).html('<div class="spinner-border spinner-border-sm mx-3" role="status"><span class="sr-only">Loading...</span></div>')

      clearAllValidation()

      $.ajax({
        type: 'POST',
        url: '{{ site_url("ajax/add-category") }}',
        dataType: 'JSON',
        data: { csrf_test_name: $('#csrf').val(), name_add_category: $('#name_add_category').val(), type_add_category: $('#type_add_category').val() },
        success: function(response){
          $('.csrf').val(response.new_hash)
          toast('bg-success', 'Sukses', '', response.message)
          $(el).removeClass('disabled').prop('disabled', false).text('Save')
          $('#form-add-category')[0].reset()
          $('#modal-add-category').modal('hide')
          table.ajax.reload();
        },
        error: function(response, textStatus, errorThrown){
          $('.csrf').val(response.responseJSON.new_hash)
          toast('bg-danger', 'Oops', response.status, response.responseJSON.message)

          if(response.status == 400){
            parseValidation(response.responseJSON.validation_errors)
          }
          $(el).removeClass('disabled').prop('disabled', false).text('Save')
        }
      });
    });
  });
</script>
@endsection