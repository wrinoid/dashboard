@extends('layouts.main')

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<style>
  .vertical-middle {
    vertical-align: middle !important;
  }
  .dtr-data {
    word-wrap: anywhere;
  }
</style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Accounting</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Journals</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header with-border">
              <h3 class="card-title">Journals</h3>
              <div class="card-tools">
                <ul class="nav nav-pills ml-auto">
                  <li class="nav-item">
                    <button class="btn btn-sm btn-primary" id="btn-add-journal"><i class="fas fa-plus"></i> Add Journal</button>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-body">
              <table id="table-journals" class="table table-striped table-bordered dt-responsive">
                <thead>
                  <tr>
                    <th style="width: 15%">Date</th>
                    <th style="width: 15%">Account</th>
                    <th style="width: 15%">Debit</th>
                    <th style="width: 15%">Credit</th>
                    <th style="width: 15%">Category</th>
                    <th style="width: 15%">Reference</th>
                    <th style="width: 10%">Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/. container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="modal-add-journal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Journal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-add-journal">
          <div class="form-group">
            <label for="type_add_journal">Type</label>
            <select id="type_add_journal" class="form-control">
              <option value="Debit">Debit</option>
              <option value="Credit">Credit</option>
              <option value="Transfer">Transfer</option>
            </select>
          </div>
          <div class="row">
            <div class="col-xs-12 col-lg-12" id="row-account-from">
              <div class="form-group">
                <label for="account_from_add_journal">Account From</label>
                <select id="account_from_add_journal" class="form-control">
                  <option></option>
                  @foreach($accounts as $account)
                  <option value="{{ $account->account_id }}">{{ $account->account_name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-lg-6" id="row-account-to" style="display: none;">
              <div class="form-group">
                <label for="account_to_add_journal">Account To</label>
                <select id="account_to_add_journal" class="form-control">
                  <option></option>
                  @foreach($accounts as $account)
                  <option value="{{ $account->account_id }}">{{ $account->account_name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="amount_add_journal">Amount</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text">Rp.</span>
              </div>
              <input type="number" class="form-control" placeholder="Amount" step="0.01" id="amount_add_journal">
            </div>
          </div>
          <div class="form-group" id="row-category">
            <label for="category_add_journal">Category</label>
            <select id="category_add_journal" class="form-control">
              <option></option>
              @foreach($categories as $category)
              <option value="{{ $category->category_id }}">{{ $category->category_name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="reference_add_journal">Reference</label>
            <input type="text" class="form-control" placeholder="Reference" id="reference_add_journal">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-add-journal">Save</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{ base_url('assets/plugins/v3.2/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/jszip/jszip.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script type="text/javascript" charset="utf8" src="{{ base_url('assets/plugins/datatables/extensions/Input/input.js') }}"></script>
<!-- Select2 -->
<script src="{{ base_url('assets/plugins/v3.2/select2/js/select2.full.min.js') }}"></script>


<script type="text/javascript">
  $(document).ready(function(){
    var table = $('#table-journals').DataTable({
      'processing': true,
      'serverSide': true,
      'responsive': {
        'details': {
          'type': 'column'
        }
      },
      'ajax': {
        'url': '{{ site_url("ajax/list-journal") }}',
        'dataType': 'json',
        'type': 'POST',
        'data': function(d){
          d.csrf_test_name = $('#csrf').val()
        }
      },
      'columns': [
        {
          'data': 'created_at',
          'className': 'vertical-middle',
          'orderable': false
        },
        {
          'data': 'account_name',
          'className': 'vertical-middle',
          'orderable': false
        },
        {
          'data': 'debit',
          'className': 'vertical-middle text-right text-red',
          'orderable': false
        },
        {
          'data': 'credit',
          'className': 'vertical-middle text-right text-green',
          'orderable': false
        },
        {
          'data': 'category_name',
          'className': 'vertical-middle',
          'orderable': false
        },
        {
          'data': 'reference',
          'className': 'vertical-middle',
          'orderable': false
        },
        {
          'data': 'action',
          'className': 'vertical-middle text-center',
          'orderable': false
        }
      ],
      'order': [ 0, 'desc' ],
      // 'pagingType': 'input',
      'drawCallback': function(settings){
        $('.csrf').val(settings.json.new_hash);
      }
    });

    $('#table-journals thead th').removeClass('text-center text-right text-green text-red');

    $('#account_from_add_journal, #account_to_add_journal').select2({
      placeholder: "Please select an account",
      dropdownParent: $("#modal-add-journal")
    });

    $('#category_add_journal').select2({
      placeholder: "Please select a category",
      dropdownParent: $("#modal-add-journal")
    });

    $('#btn-add-journal').click(function(e){
      clearAllValidation()
      $('#modal-add-journal').modal('show');
    });

    $('#type_add_journal').change(function(e){
      if ($(this).val() === 'Transfer') {
        $('#row-account-from').removeClass('col-lg-12').addClass('col-lg-6')
        $('#row-account-to').show()
        $('#row-category').hide()
      }
      else {
        $('#row-account-from').removeClass('col-lg-6').addClass('col-lg-12')
        $('#row-account-to').hide()
        $('#row-category').show()

        $('#save-add-journal').addClass('disabled').prop('disabled', true).html('<div class="spinner-border spinner-border-sm mx-3" role="status"><span class="sr-only">Loading...</span></div>')

        $.ajax({
          type: 'POST',
          url: '{{ site_url("ajax/get-category") }}',
          dataType: 'JSON',
          data: { csrf_test_name: $('#csrf').val(), type_add_journal: $('#type_add_journal').val() },
          success: function(response){
            $('.csrf').val(response.new_hash)
            var opt = "<option></option>"
            $.each(response.data, function(k, v){
              opt += "<option value='"+v.category_id+"'>"+v.category_name+"</option>"
            })
            $('#category_add_journal').html(opt).select2({
              placeholder: "Please select a category",
              dropdownParent: $("#modal-add-journal")
            })

            $('#save-add-journal').removeClass('disabled').prop('disabled', false).text('Save')
          },
          error: function(response, textStatus, errorThrown){
            $('.csrf').val(response.responseJSON.new_hash)
            toast('bg-danger', 'Oops', response.status, response.responseJSON.message)

            $('#save-add-journal').removeClass('disabled').prop('disabled', false).text('Save')
          }
        });
      }
    });

    $('#save-add-journal').click(function(e){
      var el = this;
      $(el).addClass('disabled').prop('disabled', true).html('<div class="spinner-border spinner-border-sm mx-3" role="status"><span class="sr-only">Loading...</span></div>')

      clearAllValidation()

      $.ajax({
        type: 'POST',
        url: '{{ site_url("ajax/add-journal") }}',
        dataType: 'JSON',
        data: { csrf_test_name: $('#csrf').val(), type_add_journal: $('#type_add_journal').val(), account_from_add_journal: $('#account_from_add_journal').val(), account_to_add_journal: $('#account_to_add_journal').val(), amount_add_journal: $('#amount_add_journal').val(), category_add_journal: $('#category_add_journal').val(), reference_add_journal: $('#reference_add_journal').val() },
        success: function(response){
          $('.csrf').val(response.new_hash)
          toast('bg-success', 'Sukses', '', response.message)
          $(el).removeClass('disabled').prop('disabled', false).text('Save')
          $('#form-add-journal')[0].reset()
          $('#modal-add-journal').modal('hide')
          table.ajax.reload();
        },
        error: function(response, textStatus, errorThrown){
          $('.csrf').val(response.responseJSON.new_hash)
          toast('bg-danger', 'Oops', response.status, response.responseJSON.message)

          if(response.status == 400){
            parseValidation(response.responseJSON.validation_errors)
          }
          $(el).removeClass('disabled').prop('disabled', false).text('Save')
        }
      });
    });
  });
</script>
@endsection