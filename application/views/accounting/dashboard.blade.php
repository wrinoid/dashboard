@extends('layouts.main')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Accounting</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-6 col-xs-12">
          <div class="info-box bg-black shadow">
            <span class="info-box-icon"><i class="fas fa-hand-holding-usd"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Modal</span>
              <h4>Rp. {{ number_format($modal, 2) }}</h4>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        <div class="col-lg-6 col-xs-12">
          <div class="info-box bg-black shadow">
            <span class="info-box-icon"><i class="fas fa-wallet"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Saldo <span class="text-sm ml-2 {{ $margin_percentage < 0 ? 'text-red' : 'text-green' }}">{!! ($margin_percentage < 0 ? '<i class="fas fa-caret-down"></i> '.($margin_percentage * -1) : '<i class="fas fa-caret-up"></i> '.$margin_percentage) !!}%</span></span>
              <div>
                <h4>Rp. {{ number_format($saldo, 2) }}</h4>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        <div class="col-sm-12">
          <div class="card shadow">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-chart-line"></i>
                Sales Graph - {{ Carbon\Carbon::now()->format('F Y') }}
              </h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <canvas class="chart" id="line-chart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/. container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('script')
<script>
  $(function () {
    var salesGraphChartCanvas = $('#line-chart').get(0).getContext('2d')
    // $('#revenue-chart').get(0).getContext('2d');

    var salesGraphChartData = {
      labels: {{ $line_chart['day'] }},
      datasets: [
        {
          label: 'Sales',
          fill: false,
          borderWidth: 2,
          lineTension: 0,
          spanGaps: true,
          borderColor: '#efefef',
          pointRadius: 3,
          pointHoverRadius: 7,
          pointColor: '#efefef',
          pointBackgroundColor: '#efefef',
          data: {{ $line_chart['value'] }}
        }
      ]
    }

    var salesGraphChartOptions = {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false
      },
      tooltips: {
        callbacks: {
          label: function(t, d) {
            const label = d.datasets[t.datasetIndex].label;
            const value = d.datasets[t.datasetIndex].data[t.index];
            return `${label}: Rp. ${value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
          }
        }
      },
      scales: {
        xAxes: [{
          ticks: {
            fontColor: '#efefef'
          },
          gridLines: {
            display: false,
            color: '#efefef',
            drawBorder: false
          }
        }],
        yAxes: [{
          ticks: {
            // stepSize: 5000,
            min: 0,
            fontColor: '#efefef',
            callback: function (value, index, values) {
              return 'Rp. '+value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            },
          },
          gridLines: {
            display: true,
            color: '#efefef',
            drawBorder: false
          }
        }]
      }
    }

    // This will get the first returned node in the jQuery collection.
    // eslint-disable-next-line no-unused-vars
    var salesGraphChart = new Chart(salesGraphChartCanvas, { // lgtm[js/unused-local-variable]
      type: 'line',
      data: salesGraphChartData,
      options: salesGraphChartOptions
    })
  })
</script>
@endsection