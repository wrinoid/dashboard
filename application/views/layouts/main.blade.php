<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{ $title }}</title>
  <link rel="icon" href="{{ base_url('assets/img/favicon.png') }}" type="image/png"/>
  <link rel="shortcut icon" href="{{base_url('assets/img/favicon.png') }}" type="image/png"/>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/fontawesome-free/css/all.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  @yield('css')
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ base_url('assets/css/v3.2/adminlte.min.css') }}">
  <!-- Custom style -->
  <link rel="stylesheet" href="{{ base_url('assets/css/v3.2/custom.css') }}">

  <style>
    .toasts-top-right{
      z-index: 1051;
    }

    .toast{
      min-width: 300px;
    }
  </style>
</head>
<body class="hold-transition dark-mode sidebar-mini layout-fixed layout-navbar-fixed sidebar-collapse">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__wobble" src="{{ base_url('assets/img/favicon.png') }}" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="{{ site_url('auth/logout') }}" role="button">
          <i class="fas fa-sign-out-alt"></i> Sign Out
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ site_url() }}" class="brand-link">
      <img src="{{ base_url('assets/img/favicon.png') }}" alt="wDashboard Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">wDashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ base_url('assets/img/profile/'.$CI->auth->photo) }}" class="img-circle elevation-2 profile-photo" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ site_url('profile') }}" class="d-block">{{ $CI->auth->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="{{ site_url('welcome') }}" class="nav-link{{ $menu == 'dashboard' ? ' active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item{{ in_array($menu, ['accounting_dashboard', 'accounting_account', 'accounting_category', 'accounting_journal']) ? '  menu-is-opening menu-open' : '' }}">
            <a href="#" class="nav-link{{ in_array($menu, ['accounting_dashboard', 'accounting_account', 'accounting_category', 'accounting_journal']) ? ' active' : '' }}">
              <i class="nav-icon fas fa-tshirt"></i>
              <p>
                Zettakids.id
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ site_url('accounting/dashboard') }}" class="nav-link{{ $menu == 'accounting_dashboard' ? ' active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ site_url('accounting/account') }}" class="nav-link{{ $menu == 'accounting_account' ? ' active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Accounts</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ site_url('accounting/category') }}" class="nav-link{{ $menu == 'accounting_category' ? ' active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Categories</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ site_url('accounting/journal') }}" class="nav-link{{ $menu == 'accounting_journal' ? ' active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Journals</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ site_url('log/visitor') }}" class="nav-link{{ $menu == 'log-visitor' ? ' active' : '' }}">
              <i class="nav-icon fas fa-paw"></i>
              <p>
                Log Visitor {!! '<span class="right badge badge-secondary">'.$total_visitors.'</span>' !!}
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  @yield('content')

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2018-{{ date('Y') }} <a href="https://wrino.id">wrino.id</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 4.1.1
    </div>
  </footer>
  <input type="hidden" id="csrf" class="csrf" name="csrf_test_name" value="{{ $CI->security->get_csrf_hash() }}">
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ base_url('assets/plugins/v3.2/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ base_url('assets/plugins/v3.2/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ base_url('assets/plugins/v3.2/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ base_url('assets/js/v3.2/adminlte.js') }}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ base_url('assets/plugins/v3.2/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/raphael/raphael.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ base_url('assets/plugins/v3.2/jquery-mapael/maps/usa_states.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ base_url('assets/plugins/v3.2/chart.js/Chart.min.js') }}"></script>
<script>
  var site_url = '{{ site_url() }}';

  function toast(class_name = '', title = '', subtitle = '', body = ''){
    $(document).Toasts('create', {
      class: class_name,
      title: title,
      subtitle: subtitle,
      body: body
    })
  };

  function parseValidation(errors){
    $.each(errors, function(index, value) {
      $('#'+index).addClass('is-invalid');
      $('<span id="'+index+'-error-message" class="error invalid-feedback">'+value+'</span>').insertAfter('#'+index);
    });
  };

  function clearAllValidation(){
    $('.is-invalid').removeClass('is-invalid');
    $('.invalid-feedback').remove();
  };

  function clearValidation(element){
    $('#'+element).removeClass('is-invalid');
    $('#'+element).siblings('.invalid-feedback').remove();
  };

  $('input').change(function(e){
    clearValidation($(this).attr('id'))
  })

  $('input').keyup(function(e){
    clearValidation($(this).attr('id'))
  })
</script>

@yield('script')
</body>
</html>
