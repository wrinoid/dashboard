<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Log in</title>
  <link rel="icon" href="{{ base_url('assets/img/favicon.png') }}" type="image/png">
  <link rel="shortcut icon" href="{{ base_url('assets/img/favicon.png') }}" type="image/png">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ base_url('assets/plugins/v3.2/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ base_url('assets/css/v3.2/adminlte.min.css') }}">
</head>
<body class="hold-transition login-page dark-mode">
    
@yield('content')

<!-- jQuery -->
<script src="{{ base_url('assets/plugins/v3.2/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ base_url('assets/plugins/v3.2/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ base_url('assets/js/v3.2/adminlte.min.js') }}"></script>
</body>
</html>
