<?php

if (!function_exists('slug')) {
    function slug($string = "")
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    }
}
