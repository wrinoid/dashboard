<?php

if (!function_exists('json')) {
    function json($data = [])
    {
        return json_encode($data);
    }
}

if (!function_exists('dd')) {
    function dd()
    {
        echo "<pre>";
        $args = func_get_args();
        foreach ($args as $arg) {
            var_dump($arg);
        }
        exit();
    }
}

if (!function_exists('response')) {
    function response($status_code = 200, $data = null)
    {
        http_response_code($status_code);

        if (!is_null($data) && is_array($data)) {
            header('Content-Type: application/json; charset=utf-8');
            echo json($data);
        } else {
            echo $data;
        }

        exit();
    }
}
